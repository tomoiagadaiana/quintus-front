import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';

import {LoginComponent} from './login/login.component';
import {UpdateAccountComponent} from './account/update-account/update-account.component';
import {AccountsComponent} from './account/account.component';



const routes: Routes = [
  {path: '', redirectTo: 'customer', pathMatch: 'full'},
  {path: '', component: LoginComponent},
  {path: 'account', component: AccountsComponent},
  {path: 'updateAccount', component: UpdateAccountComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class AppRoutingModule {
}
