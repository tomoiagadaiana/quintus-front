import {Component, OnInit} from '@angular/core';
import {LoginService, UserLogin} from './login.service';
import {Router} from '@angular/router';
import {MatDialog, MatDialogRef} from '@angular/material';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  userLogin: UserLogin;

  error: boolean;
  errorMessage: string;

  constructor(private loginService: LoginService,
              private router: Router) {

    this.userLogin = {
      email: '',
      password: ''
    };
    this.error = false;
    this.errorMessage = '';
  }

  displayError() {
    return this.error;
  }

  displayErrorMessage() {
    return this.errorMessage;
  }

  validateEmail() {
    const regex = new RegExp('^[A-Za-z0-9._%+-]+@gmail.com$');

    return regex.test(this.userLogin.email);
  }

  validatePassword() {
    if (this.userLogin.password.length >  8) {
      return true;
    } else {
      return false;
    }
  }

  isLoggedIn() {
    return this.loginService.isLoggedIn();
  }

  logout() {
    this.loginService.logout();
  }

  submitForm() {
    this.loginService.login(this.userLogin).subscribe(
      data => {
        this.error = false;

        //
        // this.router.navigate(['adminPage']);


      },
      err => {
        this.error = true;
        console.log(err.valueOf().error);
        if (err.valueOf().error === 'EMAIL_NOT_VALID') {
          this.errorMessage = 'Email is incorrect.';
        } else if (err.valueOf().error === 'PASSWORD_NOT_VALID') {
          this.errorMessage = 'Password is incorrect.';
        } else {
          this.errorMessage = 'nu merge';
        }

      }
    );
  }

  ngOnInit() {

  }

}
