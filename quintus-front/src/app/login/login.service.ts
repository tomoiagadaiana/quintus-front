import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {Router} from '@angular/router';
import {tap} from 'rxjs/operators';


export interface UserLogin {
  email: string;
  password: string;
}




@Injectable({
  providedIn: 'root'
})
export class LoginService {

  baseURL = 'http://localhost:8080/home/login';

  constructor(private http: HttpClient, private router: Router) {
  }

  login(userLogin: UserLogin) {
    console.log(userLogin.email);
    const params = new HttpParams().set('email', userLogin.email.toString()).set('password', userLogin.password.toString());
    return this.http.post<UserLogin>(this.baseURL, params).pipe(tap(res => this.setSession(res)));
  }

  private setSession(userData: UserLogin) {
    localStorage.setItem('email', userData.email);

  }

  public isLoggedIn() {
    if (!localStorage['email']) {
      return false;
    }
    return true;
  }

  public logout() {
    localStorage.removeItem('email');

  }


}
