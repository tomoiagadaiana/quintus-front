import {Component} from '@angular/core';
import {LoginComponent} from './login/login.component';
import {MatDialog} from '@angular/material';
import {LoginService} from './login/login.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'JavaSampleApproach';
  description = 'Angular-SpringBoot';

  constructor(private loginService: LoginService,public dialog: MatDialog) {

  }

  isLoggedIn() {
    return this.loginService.isLoggedIn();
  }
  openDialog(): void {
    const dialog = this.dialog.open(LoginComponent, {
      width: '400px'
    });
  }
  logout() {
    this.loginService.logout();
  }


}
