import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material';
import {AccountsService, AccountType} from '../account.service';
import {MatDialogRef} from '@angular/material';
import {AccountsComponent} from '../account.component';

@Component({
  selector: 'app-update-account',
  templateUrl: './update-account.component.html',
  styleUrls: ['./update-account.component.css']
})
export class UpdateAccountComponent implements OnInit {
  error: boolean;
  errorMessage: string;
  accountTypeList: AccountType[];

  constructor(@Inject(MAT_DIALOG_DATA) public data, public dialog: MatDialogRef<AccountsComponent>, private accountService: AccountsService) {
    this.error = false;
    this.errorMessage = '';
    this.allAccountsType();
  }

  updateAccount() {
    console.log('' + this.data.budget);
    console.log(this.data);
    return this.accountService.updateAccount(this.data).subscribe(
      data => {
        this.error = false;
        this.dialog.close();
      },
      error1 => {
        this.error = true;
       this.errorMessage = error1.valueOf().error;


      });
  }


  displayError() {
    return this.error;
  }

  displayErrorMessage() {
    return this.errorMessage;
  }

  allAccountsType() {

    this.accountService.allAccountsType().subscribe(
      {
        next: (value: any[]) => {
          this.accountTypeList = value;
        }
      }
    );
    console.log('listaaccount' + this.accountTypeList);
  }

  ngOnInit() {
    this.allAccountsType();
    console.log(this.accountTypeList);
  }

}
