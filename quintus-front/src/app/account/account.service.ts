import {Injectable} from '@angular/core';
import {HttpClient, HttpHandler, HttpHeaders, HttpParams} from '@angular/common/http';

export interface AccountType {
  id: number;
  title: string;
}

export interface Account {
  id: number;
  budget: number;
  accountType: AccountType;
}

@Injectable({
  providedIn: 'root'
})
export class AccountsService {

  baseURL = 'http://localhost:8080/account';

  constructor(private http: HttpClient) {
  }

  allAccounts(email) {
    const params = new HttpParams().set('email', email);
    return this.http.get <Account[]>(this.baseURL + '/allAccounts', {params});
  }

  allAccountsType() {
    return this.http.get <AccountType[]>(this.baseURL + '/allAccountsType');
  }

  updateAccount(account) {
    return this.http.put<Account>(this.baseURL + '/updateAccount', account, {
      headers: new HttpHeaders(
        {'Content-Type': 'application/json'}
      )
    });
  }
  addAccount(account) {
    return this.http.post<Account>(this.baseURL + 'addAccount', account, {
      headers: new HttpHeaders(
        {'Content-Type': 'application/json'},
      )
    });
  }

  deleteAccount(id: number) {
    const params = new HttpParams().set('id', id.toString());
    return this.http.delete(this.baseURL + '/delete', {params});
  }
}
