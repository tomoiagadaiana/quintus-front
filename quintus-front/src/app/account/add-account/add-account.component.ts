import {Component, OnInit} from '@angular/core';

import {AccountsService, AccountType, Account} from '../account.service';
import {MatDialogRef} from '@angular/material';

@Component({
  selector: 'app-add-account',
  templateUrl: './add-account.component.html',
  styleUrls: ['./add-account.component.css']
})
export class AddAccountComponent implements OnInit {

  error: boolean;
  errorMessage: string;
  accountAdded: Account;
  accountTypeList: AccountType[];
  accountType: AccountType;

  constructor(private accountService: AccountsService, public dialogRef: MatDialogRef<AddAccountComponent>) {
    this.accountType = {
      id: 1,
      title: 'economii'
    };
    this.accountAdded = {
      id: 2,
      budget: 2,
      accountType: this.accountType,
    };

  }

  allAccountsType() {

    this.accountService.allAccountsType().subscribe(
      {
        next: (value: any[]) => {
          this.accountTypeList = value;
        }
      }
    );
    console.log('listaaccount' + this.accountTypeList);
  }

  displayError() {
    return this.error;
  }

  displayErrorMessage() {
    return this.errorMessage;
  }

  addAccount() {
    return this.accountService.addAccount(this.accountAdded).subscribe(
      data => {
        this.error = false;
        this.dialogRef.close();
        this.accountService.allAccounts(localStorage.getItem('email'));
      },
      error1 => {
        this.error = true;
        this.errorMessage = error1.valueOf().error;
      }
    );
  }

  validateBudget() {
    if (this.accountAdded.budget === null || this.accountAdded.budget < 0) {
      return false;
    }
    return true;
  }

  validateAccountType() {
    if (this.accountAdded.accountType === null) {
      return true;
    }
    return false;
  }

  ngOnInit() {
  }
}
