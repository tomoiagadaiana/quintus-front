import {Component, OnInit} from '@angular/core';
import {LoginService} from '../login/login.service';
import {MatDialog} from '@angular/material';
import {UpdateAccountComponent} from './update-account/update-account.component';
import {AccountsService, AccountType, Account} from './account.service';
import {AddAccountComponent} from './add-account/add-account.component';


@Component({
  selector: 'app-account',
  templateUrl: './account.component.html',
  styleUrls: ['./account.component.css']
})
export class AccountsComponent implements OnInit {
  account: Account;
  accountType: AccountType;
  error: boolean;
  errorMessage: string;
  accountsTypeList: AccountType[];
  accountsList: Account[];

  constructor(private loginService: LoginService, public accountSevice: AccountsService, public dialog: MatDialog) {
    this.accountType = {
      id: 1,
      title: 'economii'
    };
    this.account = {
      id: 2,
      budget: 2,
      accountType: this.accountType,
    };
  }

  ngOnInit() {
    this.allAccounts();
  }

  isLoggedIn() {
    return this.loginService.isLoggedIn();
  }

  logout() {
    this.loginService.logout();
  }

  allAccounts() {
    console.log('all' + localStorage.getItem('email'));
    this.accountSevice.allAccounts(localStorage.getItem('email')).subscribe(
      {
        next: (value: any[]) => {
          this.accountsList = value;
        }
      }
    );
  }


  openUpdateDialog(account): void {
    const dialog = this.dialog.open(UpdateAccountComponent, {
      width: '400px',
      data: {
        id: account.id,
        budget: account.budget,
        accountType: account.accountType
      }

    });

    dialog.afterClosed().subscribe(
      result => {
        this.allAccounts();
      }
    );
  }

  validateAccountsType() {
    if (this.accountsTypeList === null) {
      return true;
    }
    return false;
  }


  openDialogAdd(): void {
    const dialog = this.dialog.open(AddAccountComponent, {
      width: '400px'
    });
  }
  deleteAccount(id): void {
    this.accountSevice.deleteAccount(id).subscribe(
      // window.location.reload();
    );
  }

}
